package com.example.spring_mvc.repository;

import com.example.spring_mvc.dto.PageDto;
import com.example.spring_mvc.dto.UserDto;
import com.example.spring_mvc.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {

    UserEntity findByGmail(String gmail);

}
