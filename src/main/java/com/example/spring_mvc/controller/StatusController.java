package com.example.spring_mvc.controller;

import com.example.spring_mvc.dto.StatusDto;
import com.example.spring_mvc.entities.StatusEntity;
import com.example.spring_mvc.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/status")
public class StatusController {
    private final StatusService statusService;

    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }
    @PostMapping("/add")
    public ResponseEntity<String> create(){
        return null;
    }
    @GetMapping("/find")
    public List<StatusDto> getAllStatus() {
        return statusService.getAllStatus();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStatusById(@PathVariable("id") Long id) {
        if (statusService.deleteById(id)) {
            return ResponseEntity.ok("Đã xóa thành công");
        }else {
            return ResponseEntity.ok("Người dùng không tồn tại");
        }

    }

    @PutMapping("/update/{id}")

    public ResponseEntity<String> update(@PathVariable Long id,@RequestBody StatusDto statusDto) {
        System.out.println(statusDto + "Controller");
        if (statusService.update(id,statusDto)) {
            return ResponseEntity.ok("Đã cập thành công");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("nội dung không tồn tại");
    }
}
