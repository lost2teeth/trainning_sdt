package com.example.spring_mvc.controller;

import com.example.spring_mvc.dto.PageDto;
import com.example.spring_mvc.dto.UserDto;
import com.example.spring_mvc.entities.UserEntity;
import com.example.spring_mvc.repository.UserRepository;
import com.example.spring_mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;


@RestController
public class UserController {
    @Autowired
    private UserService userService;
//    @Autowired
//    private UserRepository userRepository;
//    @RequestMapping(value = "/register", method = RequestMethod.GET)
//    public String register(Model model) {
//        model.addAttribute("message", "Hello Spring MVC Framework!");
//        return "register";
//    }
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public String addUser(Model model,@ModelAttribute UserDto user) {
//        System.out.println(user);
//        userService.createUser(user);
//        model.addAttribute("listUsers", userService.getAll());
//        return "listUser";
//    }
//    @RequestMapping(value = "user", method = RequestMethod.GET)
//    public String getAll(Model model, @RequestParam(defaultValue = "0") int page) {
//        Page<UserEntity> user = userService.getPaginatedUsers(page,4);
//        model.addAttribute("listUser",user );
//        return "listUser";
//    }
//    @RequestMapping(value = "/listUser/{id}", method = RequestMethod.GET)
//    public String getUserById(Model model, @PathVariable("id") Long id) {
//        UserDto user = userService.getUserById(id);
//        return "listUser";
//    }
//    @RequestMapping(value = "/listUser/{id}", method = RequestMethod.POST)
//    public String updateUserById(Model model, @PathVariable("id") Long id, @ModelAttribute UserEntity userEntity) {
//        UserEntity user = userService.updateUser(id,userEntity);
//        List<UserDto> listUser = userService.getAll();
//        model.addAttribute(listUser);
//        return "listUser";
//    }
//    @RequestMapping(value = "/listUser/delete/{id}", method = RequestMethod.GET)
//    public String deleteUserById(Model model, @PathVariable("id") Long id) {
//        boolean user = userService.deleteUser(id);
//        if (user){
//            return "redirect:/listUser";
//        }
//        return "listUser";
//
//    }

    @GetMapping("/users")
    public List<UserDto> getAllUser(){
         return userService.getAll() ;
    }
    @PostMapping("/users")
    public UserDto addUser(@RequestBody UserDto userDto){
         UserDto userDto1 = userService.createUser(userDto);
         return userDto1;
    }
    @PutMapping("/updateUser/{id}")
    public UserEntity updateUser(@RequestBody UserEntity userEntity, @PathVariable Long id){
        return userService.updateUser(id,userEntity);
    }
    @DeleteMapping("/deleteUser/{id}")
    public void deleteUser( @PathVariable Long id){
        userService.deleteUser(id);
    }
    @GetMapping("user")
    public Page<UserDto> getUsers(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size ) {
        return userService .getPaginatedUsers(page,size);
    }

}
