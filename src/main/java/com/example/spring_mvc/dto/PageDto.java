package com.example.spring_mvc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageDto<T> {
    private List<T> user;
    private int pageNumber;
    private int pageSize;
    private long totalElements;
}