package com.example.spring_mvc.dto;

import com.example.spring_mvc.entities.StatusEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import lombok.*;

import java.util.List;

@Data

public class UserDto {
    private int id;
    private String userName;
    private String passWord;
    private String gmail;
    private String address;
    private String phoneNumber;
    private List<StatusEntity> status;

}
