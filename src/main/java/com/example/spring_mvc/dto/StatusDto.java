package com.example.spring_mvc.dto;

import com.example.spring_mvc.entities.UserEntity;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class StatusDto {
    private Long id;
    private String title;
    private UserEntity user;
}
