package com.example.spring_mvc.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.Id;

;import java.util.List;


@Entity
@Table(name = "users")
@Data
public class UserEntity {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "username", nullable = false)
    private String userName;
    //    @JsonIgnore
//    ẩn hoàn toàn trường này
//    Hoặc
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    cho phép client gửi dữ liệu lên sever
    @Column(name = "password", nullable = false)
    private String passWord;
    @Column(name = "gmail", nullable = false)
    private String gmail;
    @Column(name = "phoneNumber", nullable = false)
    private String phoneNumber;
    @Column(name = "address", nullable = false)
    private String address;
    @JsonIgnore
    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER)
    private List<StatusEntity> status;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<RoleEntity> role;
}
