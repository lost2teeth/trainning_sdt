package com.example.spring_mvc.entities;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "status")
@Data
public class StatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "title")
    private String title;
    @ManyToOne(fetch = FetchType.EAGER)
    private UserEntity user;


}
