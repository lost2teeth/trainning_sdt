package com.example.spring_mvc.service;

import com.example.spring_mvc.dto.StatusDto;

import java.util.List;

public interface StatusServiceImp {
    StatusDto create(StatusDto statusDto);

    List<StatusDto> getAllStatus();

    boolean deleteById(Long id);
    boolean update(Long id, StatusDto statusDto);
}
