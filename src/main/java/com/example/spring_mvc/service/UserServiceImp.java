package com.example.spring_mvc.service;

import com.example.spring_mvc.dto.UserDto;
import com.example.spring_mvc.entities.UserEntity;

import java.util.List;

public interface UserServiceImp {
    List<UserDto> getAll();

    UserDto createUser(UserDto user);

    boolean deleteUser(Long id);

    UserEntity updateUser(Long id, UserEntity user);
    boolean login(String userName, String passWord);



}
