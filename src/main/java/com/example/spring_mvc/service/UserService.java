package com.example.spring_mvc.service;

import com.example.spring_mvc.dto.UserDto;
import com.example.spring_mvc.entities.UserEntity;
import com.example.spring_mvc.mapper.UserMapper;
import com.example.spring_mvc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements UserServiceImp {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    @Autowired
    public UserService(UserMapper userMapper, UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDto> getAll() {
        return userRepository.findAll().stream().map(userMapper::convertToDTO).collect(Collectors.toList());

    }

    @Override
    public UserDto createUser(UserDto user) {
        UserEntity userEntity = userMapper.convertToEntity(user);
        UserDto userDto = userMapper.convertToDTO(userRepository.save(userEntity));
        return userDto;
    }

    @Override
    public boolean deleteUser(Long id) {
        userRepository.deleteById(id);
        return true;
    }


    @Override
    public UserEntity updateUser(Long id, UserEntity userDto) {

        UserEntity user = userRepository.findById(id).orElseThrow();

        user.setUserName(userDto.getUserName());
        user.setAddress(userDto.getAddress());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setGmail(userDto.getGmail());
        return userRepository.save(user);


    }

    @Override
    public boolean login(String username, String password) {
        UserDto userDto = userMapper.convertToDTO(userRepository.findByGmail(username)) ;
        if (userDto != null && userDto.getPassWord().equals(password)) {
            // Đăng nhập thành công
            return true;
        } else {
            // Đăng nhập thất bại
            return false;
        }
    }
    //Phân trang

    public Page<UserDto>  getPaginatedUsers(int pageNo, int pageSize){
        PageRequest pageable = PageRequest.of(pageNo, pageSize);
        Page<UserEntity> userEntityPage = userRepository.findAll(pageable);
        return  userEntityPage.map(userEntity -> userMapper.convertToDTO(userEntity));
    }



}
