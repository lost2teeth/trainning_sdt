package com.example.spring_mvc.service;

import com.example.spring_mvc.dto.StatusDto;
import com.example.spring_mvc.entities.StatusEntity;
import com.example.spring_mvc.mapper.StatusMapper;
import com.example.spring_mvc.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatusService implements StatusServiceImp{
    private final StatusRepository statusRepository;
    private final StatusMapper statusMapper;
    @Autowired
    public StatusService(StatusRepository statusRepository, StatusMapper statusMapper) {
        this.statusRepository = statusRepository;
        this.statusMapper = statusMapper;
    }

    @Override
    public StatusDto create(StatusDto statusDto) {
        return null;
    }

    @Override
    public List<StatusDto> getAllStatus() {
        return statusRepository.findAll().stream().map(statusMapper::convertToDTO).toList();
    }

    @Override
    public boolean deleteById(Long id) {
        Optional<StatusEntity> status = statusRepository.findById(id);
        if(status.isPresent()){
            StatusDto statusDto = statusMapper.convertToDTO(status.get());
            StatusEntity statusEntity = statusMapper.convertToEntity(statusDto);
            statusRepository.delete(statusEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Long id, StatusDto statusDto) {
        System.out.println(statusDto);
        //Xử lý các trường hợp null
        Optional<StatusEntity> status = statusRepository.findById(id);
        //Kiểm tra cái optional có tồn tại hay không
        if(status.isPresent() ){
            // nếu cái statusOptional.get() để lấy giá trị mà không tồn tại bị l
            StatusDto statusDto1 = statusMapper.convertToDTO(status.get());
            statusDto1.setTitle(statusDto.getTitle());
            StatusEntity statusEntity = statusMapper.convertToEntity(statusDto1);
            statusRepository.save(statusEntity);
            return true;
        }
        return false;

    }
}
