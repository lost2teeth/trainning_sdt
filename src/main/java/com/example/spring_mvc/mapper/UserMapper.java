package com.example.spring_mvc.mapper;

import com.example.spring_mvc.dto.UserDto;
import com.example.spring_mvc.entities.UserEntity;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private static final ModelMapper modelMapper = new ModelMapper();

    //    Truyền dữ liệu từ Entity qua DTO
    public UserDto convertToDTO(UserEntity userEntity) {

        UserDto userDTO = modelMapper.map(userEntity, UserDto.class);
        return userDTO;
    }
    public Page<UserDto> convertToDTOPage(Page<UserEntity> userEntityPage) {

        Page<UserDto> page = (Page<UserDto>) modelMapper.map(userEntityPage, UserDto.class);
        return page;
    }


    //      ngược lai
    public  UserEntity convertToEntity(UserDto userDto) {
        UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
        return userEntity;
    }
}

