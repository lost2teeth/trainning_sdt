package com.example.spring_mvc.mapper;

import com.example.spring_mvc.dto.StatusDto;
import com.example.spring_mvc.entities.StatusEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class StatusMapper {
    private static final ModelMapper modelMapper = new ModelMapper();

    //    Truyền dữ liệu từ Entity qua DTO
    public StatusDto convertToDTO(StatusEntity statusEntity) {

        StatusDto statusDto = modelMapper.map(statusEntity , StatusDto.class);
        return statusDto;
    }

    //      ngược lai
    public  StatusEntity convertToEntity(StatusDto statusDto) {
        StatusEntity statusEntity = modelMapper.map(statusDto, StatusEntity.class);
        return statusEntity;
    }
}
